
import { Routes, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/styles';
import '../App';

// Pages
import SplashScreen from '../pages/splashScreen';
import HomeScreen from '../pages/homeScreen';
import AwardScreen from '../pages/awardScreen';

// NavBar
import NavBar from '../components/navBar';

const useStyles = makeStyles({
  root: {
    width: '100%',
  },
});

const Router = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <NavBar />
      <Routes>
        <Route path="/" element={<SplashScreen />} />
        <Route path="/home" element={<HomeScreen />} />
        <Route path="/award" element={<AwardScreen />} />
      </Routes>
    </div>
  );
};

export default Router;
