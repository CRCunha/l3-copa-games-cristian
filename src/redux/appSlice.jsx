import { createSlice } from '@reduxjs/toolkit';

export const slice = createSlice({
  name: 'app',
  initialState: {
    selectedGames: [],
  },

  reducers: {
    changeSelectedItems(state, { payload }) {
      if (state.selectedGames.indexOf(payload) > -1) {
        const newList = state.selectedGames.filter(item => item !== payload);
        return {selectedGames: [...newList]};
      } else {
        return { ...state, selectedGames: [...state.selectedGames, payload]};
      }
    },
    clearSelectedItems(state) {
      return {selectedGames: []};
    },
  },
});

export const {
  changeSelectedItems,
  clearSelectedItems
} = slice.actions;

export const selectApp = (state) => state.app;

export default slice.reducer;
