// Primary
export const metallicBlue = '#334D80';
export const eucalyptus = '#37D2A9';

// Secondary
export const paleViolet = '#B485FF';
export const tangerine = '#FFCD05';
export const tigersEye = '#DD8945';
export const vistaBlue = '#8099E6';

// Neutral
export const chineseBlack = '#111418';
export const white = '#FFFFFF';
export const background = '#E5E5E5';

// Buttons
export const buttonPrimaryBackground = '#37D2A9';
export const buttonPrimaryText = chineseBlack;
export const buttonHoverBackground = '#33BE99';
export const buttonHoverText = chineseBlack;
export const buttonDisabledBackground = '#C6C6C6';
export const buttonDisabledText = '#7E7E7E';

// Text
export const homeGoBackButton = '#37414E';
