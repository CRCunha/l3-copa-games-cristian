import {useState, useEffect} from 'react';
import clsx from 'clsx';
import useStyles from './styles';
import { changeSelectedItems } from '../../redux/appSlice';
import { useDispatch, useSelector } from 'react-redux';
interface CardProps  {
  id: number;
  name: string,
  platform: string,
  year: number,
  image: string,
}

const Card = (props: CardProps) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const app = useSelector((state: any) => state.app);
  const [isSelected, setIsSelected] = useState(false);

  const toggleSelected = () => {
    setIsSelected(!isSelected);
    dispatch(changeSelectedItems(props.id));
  }

  return (
    <div className={classes.cardContainer}>
      {isSelected === true || app.selectedGames.length < 8 ? (
        <div onClick={toggleSelected}
          className={clsx(
            isSelected ? classes.cardContentImageSelected : classes.cardContentImage,
          )}>
          <img src={props.image} alt="imagem do jogo" />
        </div>
      ) : (
        <div
          className={classes.cardContentImageDesable}>
          <img src={props.image} alt="imagem do jogo" />
        </div>
      ) }
      <div className={classes.cardContentTitle}>{props.name}</div>
      <div className={classes.cardContentPlatform}>{props.year} / {props.platform}</div>
    </div>
  )
}

export default Card
