import { makeStyles } from '@material-ui/styles';
import {metallicBlue, chineseBlack, paleViolet} from '../../configs/colors';

const useStyles = makeStyles({

  cardContainer: {
    width: '175px',
    height: '300px',
    marginBottom: '22px',
  },
  cardContentImage: {
    width: '100%',
    height: '220px',
    backgroundColor: '//#region fff',
    borderRadius: '5px',
    objectFit: 'cover',
    cursor: 'pointer',
    border: "5px solid #fff",

    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
      borderRadius: '5px',
    },
  },
  cardContentImageSelected: {
    width: '100%',
    height: '220px',
    backgroundColor: '#fff',
    borderRadius: '5px',
    objectFit: 'cover',
    cursor: 'pointer',
    border: `5px solid ${paleViolet}`,

    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
      borderRadius: '5px',
    },
  },

  cardContentImageDesable: {
    width: '100%',
    height: '220px',
    backgroundColor: '#fff',
    borderRadius: '5px',
    objectFit: 'cover',
    cursor: 'pointer',
    border: '5px solid #fff',
    filter: 'opacity(50%)',

    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
      borderRadius: '5px',
    },
  },
  cardContentTitle: {
    width: '100%',
    marginTop: '15px',
    fontWeight: 'bold',
    color: chineseBlack,
    fontSize: '18px',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    textAlign: 'left'
  },
  cardContentPlatform: {
    width: '100%',
    height: '30px',
    color: chineseBlack,
    fontSize: '14px',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
    textAlign: 'left'
  },

  "@media (max-width: 1120px)":{
    cardContainer: {
      width: '158px',
    }
  },
  "@media (max-width: 1445px)":{
    cardContainer: {
      width: '148px',
      height: '300px',
    },
    cardContentImage: {
      height: '220px',
    },
    cardContentImageSelected: {
      height: '220px',
    }
  }
});

export default useStyles;
