import { makeStyles } from '@material-ui/styles';
import {buttonPrimaryBackground, buttonHoverBackground, chineseBlack,buttonDisabledBackground, buttonDisabledText,} from '../../configs/colors';

const useStyles = makeStyles({
  buttonContainer: {
    borderRadius: '16px',
    padding: '16px 32px',
    backgroundColor: buttonPrimaryBackground,
    color: chineseBlack,
    fontSize: '14px',
    fontWeight: 500,
    transition: 'all 0.2s ease-in-out',
    border: 'none',
    outline: 'none',
    '&:hover': {
      backgroundColor: buttonHoverBackground,
      transition: 'all 0.2s ease-in-out',
    },
  },
  desableButtonContainer: {
    borderRadius: '16px',
    padding: '16px 32px',
    backgroundColor: buttonDisabledBackground,
    color: buttonDisabledText,
    fontSize: '14px',
    fontWeight: 500,
    border: 'none',
    outline: 'none',
    pointerEvents: 'none',
  },
});

export default useStyles;

