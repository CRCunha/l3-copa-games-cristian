import useStyles from './styles';
import { useNavigate } from 'react-router-dom';
import clsx from 'clsx';

interface ButtonProps  {
  text: string,
  disable: boolean,
  linkTo: string,
}

const ButtonComponent = (props: ButtonProps) => {
  const classes = useStyles();
  const navigate = useNavigate();


  return (
    <button onClick={() => {!props.disable ? navigate(`${props.linkTo}`) : null}}
    className={clsx(props.disable ? classes.desableButtonContainer : classes.buttonContainer)}>
      {props.text}
    </button>
  )
}

export default ButtonComponent
