import { makeStyles } from '@material-ui/styles';

const useStyles = makeStyles({
  dropDownMenuContainer: {
    '& img': {
      width: '35px'
    }
  },
  socialMedia: {
    width: '19px',
    marginLeft: '32px',
    filter: 'brightness(0.5)',
  },
});

export default useStyles;
