import {useState} from 'react'
import useStyles from './styles';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';

// Images
import TwitterLogo from '../../assets/images/twitter.svg';
import FacebookLogo from '../../assets/images/facebook.svg';
import InstagramLogo from '../../assets/images/instagram.svg';
import MenuIcon from '../../assets/images/menuIcon.svg';

const DropDownMenu = () => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.dropDownMenuContainer}>
      <img src={MenuIcon} alt="Menu Icon" onClick={handleClick} />

      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={handleClose}>Sobre</MenuItem>
        <MenuItem onClick={handleClose}>My Contato</MenuItem>
        <MenuItem onClick={handleClose}>
          <img className={classes.socialMedia} src={TwitterLogo} alt="Twitter Logo" />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <img className={classes.socialMedia} src={FacebookLogo} alt="Twitter Logo" />
        </MenuItem>
        <MenuItem onClick={handleClose}>
          <img className={classes.socialMedia} src={InstagramLogo} alt="Twitter Logo" />
        </MenuItem>
      </Menu>
    </div>
  );
};

export default DropDownMenu;
