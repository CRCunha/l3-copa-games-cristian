import useStyles from './styles';
import Grid from '@mui/material/Grid';
import Hidden from '@mui/material/Hidden';

// Compónents
import DropDownMenu from '../dropDownMenu';

// Images
import LambdaLogoFull from '../../assets/images/lambdaLogoFull.svg';
import TwitterLogo from '../../assets/images/twitter.svg';
import FacebookLogo from '../../assets/images/facebook.svg';
import InstagramLogo from '../../assets/images/instagram.svg';

const NavBar = () => {
  const classes = useStyles();
  return (
    <div className={classes.navBarContainer}>
      <div className={classes.navBarContent}>
        <Grid justifyContent="center" container item xs={11} md={11} lg={11} xl={10}>
          <Grid container alignItems="center" spacing={2}>
              <Grid item xs={2}>
                <Grid
                  sx={{
                    '& img': {
                      width: {xs:'100px',sm: '140px', lg: '180px'},
                    },
                  }}
                  container justifyContent="flex-start">
                  <img src={LambdaLogoFull} alt="Lambda Logo Header" />
                </Grid>
              </Grid>
              <Grid item xs={10}>
                <Grid container justifyContent="flex-end">
                  <Hidden mdDown>
                    <Grid xs={12} item>
                      <Grid container justifyContent="space-between">
                        <Grid item container xs={9}>
                          <div className={classes.headerTextButton}>Sobre</div>
                          <div className={classes.headerTextButton}>Contato</div>
                        </Grid>
                        <Grid item container justifyContent="flex-end" xs={3}>
                          <img className={classes.socialMedia} src={TwitterLogo} alt="Twitter Logo" />
                          <img className={classes.socialMedia} src={FacebookLogo} alt="Facebook Logo" />
                          <img className={classes.socialMedia} src={InstagramLogo} alt="Instagram Logo" />
                        </Grid>
                      </Grid>
                    </Grid>
                </Hidden>
                <Hidden mdUp>
                  <Grid xs={12} item >
                    <Grid container justifyContent="flex-end">
                      <DropDownMenu />
                    </Grid>
                  </Grid>
                </Hidden>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default NavBar;
