import { makeStyles } from '@material-ui/styles';
import {metallicBlue} from '../../configs/colors';

const useStyles = makeStyles({
  navBarContainer: {
    width: '100%',
    height: '90px',
    backgroundColor: metallicBlue,
    boxShadow: '0px 4px 56px rgba(0, 0, 0, 0.1)',
    display: 'flex',
    justifyContent: 'center',
    marginBottom: '-90px',
    position: 'fixed',
    zIndex: 100,
  },
  navBarContent: {
    width: '100%',
    height: '100%',
    padding: '0 25px',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
  },
  headerTextButton: {
    width: 'max-content',
    height: '100%',
    fontWeight: 400,
    fontSize: '18px',
    marginLeft: '50px',
  },
  socialMedia: {
    width: '19px',
    marginLeft: '32px',
  },
});

export default useStyles;
