import {useState, useEffect} from 'react';
import useStyles from './styles';
import Grid from '@mui/material/Grid';
import { useNavigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import Winner from './winner';
import Winners from './winners';
import { motion } from "framer-motion"
import ArrowLeft from '../../assets/images/arrowLeft.svg';
import Hidden from '@mui/material/Hidden';
import ButtonComponent from '../../components/ButtonComponent';

const AwardScreen = () => {
  const [first, setFirit] = useState(0);
  const [second, setSecond] = useState(0);
  const [changeScreen, setChangeScreen] = useState(false);
  const app = useSelector((state: any) => state.app);
  const selectedGames = app.selectedGames;
  const navigate = useNavigate();

  const selectWinners = () => {
    let firstWinner = (selectedGames[Math.floor(Math.random()*selectedGames.length)]) -1;
    let secondWinner = (selectedGames[Math.floor(Math.random()*selectedGames.length)]) -1;
    if(firstWinner === secondWinner){
      secondWinner = secondWinner+1;
    }
    setFirit(firstWinner);
    setSecond(secondWinner);
  }

  useEffect(() => {
    selectWinners()
  }, []);


  setTimeout(() => setChangeScreen(true), 4000);

  const classes = useStyles();
  return (
    <div className={classes.homeScreenContainer}>
      <div className={classes.homeScreenContent}>
        <motion.div
            className={classes.headerPage}
            animate={{
              opacity: [0, 1],
            }}
            transition={{ delay: 7.5 }}
          >
            <Grid container justifyContent="center" alignItems='center'>
                <div onClick={() => navigate('/')} className={classes.buttonGoBack}>
                  <img src={ArrowLeft} alt="arrowLeft" />
                  <Hidden mdDown>Voltar para o início</Hidden>
                </div>
                <div className={classes.headerItemText}>
                  Seu resultado!
                </div>
                <div className={classes.alignSelection}/>
            </Grid>
          </motion.div>
        <Grid className={classes.cardsContainer} container item xs={12}>
          <div className={classes.cardsContent}>
            {!changeScreen ? <Winner first={first} /> : <Winners first={first} second={second} /> }
          </div>
        </Grid>
        <motion.div
          animate={{
            opacity: [0, 1],
          }}
          transition={{ delay: 7.5 }}
        >
          <div className={classes.buttonContainer}>
            <ButtonComponent text="Refazer campeonato" disable={false} linkTo={'/home'} />
          </div>
        </motion.div>
      </div>
    </div>
  )
}

export default AwardScreen
