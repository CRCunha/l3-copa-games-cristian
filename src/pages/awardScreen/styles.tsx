import { makeStyles } from '@material-ui/styles';
import {metallicBlue, vistaBlue, homeGoBackButton} from '../../configs/colors';

const useStyles = makeStyles({
  homeScreenContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: metallicBlue,
  },
  homeScreenContent: {
    overflow: 'auto',
    width: '100%',
    minHeight: '100vh',
    display: 'flex',
    padding: '0 25px',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
  },
  cardFirstWinnerContainer: {
    width: '268px',
  },
  cardSecondWinnerContainer: {
    marginLeft: '90px',
    width: '215px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },
  cardFirstWinnerName: {
    marginTop: '32px',
    marginBottom: '16px',
    width: '100%',
    height: '50px',

    fontWeight: 'bold',
    fontSize: '28px',
  },
  cardFirstWinnerImage:{
    width: '258px',
    height: '365px',
    border: `solid 5px ${vistaBlue}`,
    borderRadius: '5px',
    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
  },
  cardSecondWinnerImage:{
    width: '215px',
    height: '303px',
    border: `solid 5px ${vistaBlue}`,
    borderRadius: '5px',
    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
  },
  winnersContainer: {
    display: 'flex',
    flexDirection: 'row',
  },
  headerPage: {
    marginTop: '70px',
    width: '100%',
    height: 'auto',
    flexWrap: 'wrap',

    padding: '60px 0 30px 0px',
  },
  buttonGoBack: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    '& img': {
      marginRight: '20px',
      width: '14px',
      filter: 'brightness(100)',
    },
    color: '#fff',
    fontSize: '14px',
    fontWeight: 'bolder',
    flex: 1,
  },
  headerItemText: {
    minWidth: '250px',
    flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '35px',
  },
  alignSelection: {
    flex: 1,
  },
  cardsContainer: {
    width: '100%',
    height: '700px',
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  cardsContent: {
    height: '100%',
  },
  buttonContainer: {
    width: '100%',
    marginTop: '-100px !important',
  },

  "@media (max-width: 900px)":{
    headerItemText: {
      height: '70px',
      fontSize: '24px',
    },
    buttonContainer: {
      width: '100%',
      position: 'fixed',
      left: '0px',
      bottom: '40px',
    },
    headerPage: {
      position: 'fixed',
      top: '0px',
      padding: '30px 0 20px 20px',
    },
    buttonGoBack: {
      justifyContent: 'center',
    },
    cardsContainer: {
      height: 'auto',
    }
  }
});

export default useStyles;
