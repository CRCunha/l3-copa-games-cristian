import useStyles from './styles';
import { motion } from "framer-motion"
import gameList from '../../assets/data/games.json'

interface WinnerProps  {
  first: number;
}

const Winner = (props: WinnerProps) => {
  let data = gameList.data;

  const classes = useStyles();
  return (
    <div className={classes.cardFirstWinnerContainer}>
      <motion.div
        animate={{
          opacity: [0, 1],
          scale: [0.4, 1],
        }}
      >
        <div className={classes.cardFirstWinnerImage}>
          <img src={data[props.first].image} alt="game" />
        </div>
      </motion.div>
      <motion.div
        animate={{
          opacity: [0, 1],
        }}
      >
        <div className={classes.cardFirstWinnerName}>{data[props.first].name}</div>
        <div>{data[props.first].year} / {data[props.first].platform}</div>
      </motion.div>
    </div>
  )
}

export default Winner
