import {useState} from 'react';
import useStyles from './stylesWinners';
import { motion } from "framer-motion"
import gameList from '../../assets/data/games.json'

//Images
import star1 from '../../assets/images/Star1.svg'

interface WinnerProps  {
  first: number;
  second: number;
}


const Winners = (props: WinnerProps) => {
  let data = gameList.data;
  const [changeScreen, setChangeScreen] = useState(true);

  setTimeout(() => setChangeScreen(false), 500);

  const classes = useStyles();
  return (
    <motion.div
      animate={{
        opacity: [0, 1],
        scale: [0.4, 1],
      }}
      transition={{ delay: 0.6 }}
    >
      <div className={classes.winnersContainer}>
        {changeScreen ? (<></>) : (
          <>
            <div className={classes.cardFirstWinnerContainer}>
              <motion.div
                animate={{
                  opacity: [0, 1],
                  scale: [0.4, 1],
                }}
              >
                <div className={classes.starImageContainer}>
                  <motion.div
                    animate={{
                      opacity: [0, 1],
                      scale: [0.4, 1],
                    }}
                    transition={{ delay: 1.2 }}
                  >
                    <img src={star1} alt="star" />
                    <div className={classes.textStar}>1</div>
                  </motion.div>
                </div>
                <div className={classes.cardFirstWinnerImage}>
                  <img src={data[props.first].image} alt="game" />
                </div>
              </motion.div>
              <motion.div
                animate={{
                  opacity: [0, 1],
                }}
              >
                <div className={classes.cardFirstWinnerPosition}>Primeiro Lugar</div>
                <div className={classes.cardFirstWinnerName}>{data[props.first].name}</div>
                <div className={classes.cardFirstWinnerPlatform}>{data[props.first].year} / {data[props.first].platform}</div>
              </motion.div>
            </div>

            <div className={classes.cardSecondWinnerContainer}>
              <motion.div
                animate={{
                  opacity: [0, 1],
                  scale: [0.4, 1],
                }}
              >
                <div className={classes.starImageContainerSecond}>
                  <motion.div
                    animate={{
                      opacity: [0, 1],
                      scale: [0.4, 1],
                    }}
                    transition={{ delay: 1.2 }}
                  >
                    <img src={star1} alt="star" id="starSecond" />
                    <div className={classes.textStarSecond}>2</div>
                  </motion.div>
                </div>
                <div className={classes.cardSecondWinnerImage}>
                  <img src={data[props.second].image} alt="game" />
                </div>
              </motion.div>
              <motion.div
                animate={{
                  opacity: [0, 1],
                }}
                style={{width: '100%'}}
              >
                <div className={classes.cardFirstWinnerPosition}>Segundo Lugar</div>
                <div className={classes.cardFirstWinnerName}>{data[props.second].name}</div>
                <div className={classes.cardFirstWinnerPlatform}>{data[props.second].year} / {data[props.second].platform}</div>
              </motion.div>
            </div>
          </>
        )}
      </div>
    </motion.div>
  )
}

export default Winners
