import { makeStyles } from '@material-ui/styles';
import {chineseBlack, vistaBlue} from '../../configs/colors';

const useStyles = makeStyles({
  cardFirstWinnerContainer: {
    width: '268px',
  },
    cardSecondWinnerContainer: {
    marginLeft: '90px',
    width: '215px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },
  cardFirstWinnerName: {
    marginTop: '6px',
    marginBottom: '8px',
    width: '100%',

    fontWeight: 'bold',
    fontSize: '24px',
    textAlign: 'left',
  },
  cardFirstWinnerPlatform: {
    width: '100%',
    textAlign: 'left',
  },
  cardFirstWinnerImage:{
    width: '258px',
    height: '365px',
    border: `solid 5px ${vistaBlue}`,
    borderRadius: '5px',
    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
  },
  starImageContainer:{
    width: '61px', height: '61px',
    position: 'relative',
    top: '50px',
    left: '-26px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  starImageContainerSecond:{
    width: '48px', height: '48px',
    position: 'relative',
    top: '44px',
    left: '-20px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',

    '& img': { width: '100%', height: '100%', objectFit: 'cover', },
  },
  textStarSecond:{
    position: 'relative',
    top: '-43px',
    left: '0%',
    color: chineseBlack,
    fontWeight: 'bold',
    fontSize: '18px',
  },
  textStar:{
    top: '-50px',
    position: 'relative',
    left: '0%',
    color: chineseBlack,
    fontWeight: 'bold',
    fontSize: '24px',
  },
  cardSecondWinnerImage:{
    width: '215px',
    height: '303px',
    border: `solid 5px ${vistaBlue}`,
    borderRadius: '5px',
    '& img': {
      width: '100%',
      height: '100%',
      objectFit: 'cover',
    },
  },
  cardFirstWinnerPosition: {
    padding: '20px 0 10px 0',
    width: '100%',
    textAlign: 'start',
  },
  winnersContainer: {
    display: 'flex',
    flexDirection: 'row',
  },

  "@media (max-width: 900px)":{
    winnersContainer: {
      width: '300px',
    },
    cardSecondWinnerContainer: {
      width: '300px',
      padding: '0px 30px 0px 0px'
    },
  }
});

export default useStyles;
