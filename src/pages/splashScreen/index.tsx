import useStyles from './styles';
import Grid from '@mui/material/Grid';

// Images
import IlustracaoFull from '../../assets/images/ilustracaoFull.svg';

// Components
import ButtonComponent from '../../components/ButtonComponent';

const SplashScreen = () => {
  const classes = useStyles();
  return (
    <div className={classes.splashScreenContainer}>
      <div className={classes.splashScreenContent}>
        <div className={classes.splashScreenAssideLeft}>
          <div className={classes.tittleTopText}>Boas vindas a</div>
          <div className={classes.tittleMidText}>Copa<strong>Games</strong></div>
          <div className={classes.tittleBotText}>
            A  Lambda3  está  organizando  uma  Copa  do  Mundo  de games,  e  precisamos  de  sua  ajuda  para determinar quais gamesestarão no pódio e quem é o grande campeão. O resultado de cada partida é determinado de acordo com a nota do público para cada um dos games.
          </div>
          <div className={classes.buttonContainer}>
            <ButtonComponent text="Quero começar!" disable={false} linkTo="/home" />
          </div>
        </div>
        <div className={classes.splashScreenAssideRight}>
          <img src={IlustracaoFull} alt="Ilustração" />
        </div>
      </div>
    </div>
  )
}

export default SplashScreen
