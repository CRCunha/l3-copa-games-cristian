import { makeStyles } from '@material-ui/styles';
import {metallicBlue} from '../../configs/colors';

const useStyles = makeStyles({
  splashScreenContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  splashScreenContent: {
    width: '100%',
    minHeight: '100vh',
    overflow: 'auto',

    display: 'flex',
  },
  splashScreenAssideLeft: {
    width: '50%',
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  splashScreenAssideRight: {
    width: '50%',
    height: '100%',

    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    '& img': {
      width: '80%',
    },
  },
  tittleTopText: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '80%',
    fontSize: '36px',
    marginBottom: '60px',
  },
  tittleMidText: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: '56px',
    width: '80%',
    marginBottom: '60px',
  },
  tittleBotText: {
    display: 'flex',
    justifyContent: 'flex-start',
    fontSize: '18px',
    width: '80%',
    textAlign: 'left',
    marginBottom: '60px',
    lineHeight: '100%',
  },
  buttonContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    width: '80%',
  },

  "@media (max-width: 900px)": {
    splashScreenAssideLeft: {
      width: '100%',
      marginTop: '-50px',
    },
    splashScreenAssideRight: {
      width: '100% ',
    },
    splashScreenContent: {
      display: 'flex',
      flexDirection: 'column-reverse',
    },
    tittleTopText: {
      fontSize: '24px',
      marginBottom: '15px',
    },
    tittleMidText: {
      fontSize: '44px',
      marginBottom: '25px',
    },
    tittleBotText: {
      marginBottom: '60px',
      fontSize: '12px',

    },
    buttonContainer: {
      justifyContent: 'center',
    }
  },
});

export default useStyles;

