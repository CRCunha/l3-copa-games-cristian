import {useEffect} from 'react';
import useStyles from './styles';
import Grid from '@mui/material/Grid';
import Hidden from '@mui/material/Hidden';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { clearSelectedItems } from '../../redux/appSlice';
import ButtonComponent from '../../components/ButtonComponent';
import CopaGames from '../../assets/images/copaGames.svg';
import ArrowLeft from '../../assets/images/arrowLeft.svg';
import Card from '../../components/card';

import gameList from '../../assets/data/games.json'

const HomeScreen = () => {
  const app = useSelector((state: any) => state.app);
  const dispatch = useDispatch();
  let data = gameList.data;
  const navigate = useNavigate();
  const classes = useStyles();

  useEffect(() => {
    dispatch(clearSelectedItems());
  }, []);

  return (
    <div className={classes.homeScreenContainer}>
      <div className={classes.homeScreenContent}>
        <Grid justifyContent="center" container item xs={11} md={11} lg={11} xl={10}>
          <div className={classes.headerPage}>
            <Grid container justifyContent="center">
              <div onClick={() => navigate('/')} className={classes.buttonGoBack}>
                <img src={ArrowLeft} alt="arrowLeft" />
                <Hidden mdDown>Voltar para o início</Hidden>
              </div>
              <div className={classes.headerItemLogo}>
                <img src={CopaGames} alt="Copa Games" />
              </div>
              <div className={classes.alignSelection}/>
            </Grid>
            <Grid container justifyContent="center">
              <div className={classes.tittleSelection}>
                1.Seleção
              </div>
              <div className={classes.textSelection}>
                Para começar, selecione 8 jogos para entrar na competição e depois clique em Gerar Campeonato para prosseguir.
              </div>
              <div className={classes.alignSelection}/>
            </Grid>
            <div className={classes.selectionInfosContainer}>
              <div className={classes.textSelectionCounter}> <span>{app.selectedGames.length} de 8</span>Selecionados</div>
                <Hidden mdDown>
                  <ButtonComponent text="Gerar Campeonato" disable={app.selectedGames.length < 8 ? true : false} linkTo="/award" />
                </Hidden>
            </div>
          </div>
        </Grid>
        <Grid justifyContent="center" container item xs={12} md={12} lg={11} xl={10}>
          <div className={classes.containerCards}>
            {data.map((value) => {
              return(
                <Card key={value.id} id={value.id} name={value.name} platform={value.platform} year={value.year} image={value.image} />
              )
            })}
          </div>
        </Grid>
        <Hidden mdUp>
          <div className={classes.buttonContainerMob}>
            <ButtonComponent text="Gerar Campeonato" disable={app.selectedGames.length < 8 ? true : false} linkTo="/award" />
          </div>
        </Hidden>
      </div>
    </div>
  )
}

export default HomeScreen
