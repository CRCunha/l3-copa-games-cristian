import { makeStyles } from '@material-ui/styles';
import {white, homeGoBackButton, metallicBlue, chineseBlack, eucalyptus} from '../../configs/colors';

const useStyles = makeStyles({
  homeScreenContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: white,
  },
  homeScreenContent: {
    overflow: 'auto',
    width: '100%',
    minHeight: '100vh',
    display: 'flex',
    padding: '0 25px',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
  headerPage: {
    marginTop: '70px',
    width: '100%',
    height: 'auto',
    display: 'flex',
    flexWrap: 'wrap',
    '& img': {
      padding: '60px 0px',
      width: '220px',
    },
  },
  headerItem: {
    width: '100%',
    height: '100%',
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  buttonGoBack: {
    width: '100%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer',
    '& img': {
      marginRight: '20px',
      width: '14px',
    },
    color: homeGoBackButton,
    fontSize: '14px',
    fontWeight: 'bolder',
    flex: 1,
  },
  headerItemLogo: {flex: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  tittleSelection: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
    fontSize: '34px',
    fontWeight: 'bold',
    color: metallicBlue,
    flex: 1,
  },
  textSelection: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '16px',
    color: chineseBlack,
    lineHeight: '100%',
    '& span': {color: eucalyptus, fontWeight: 'bold', marginRight: '10px'},
  },
  textSelectionCounter: {
    display: 'flex',
    alignItems: 'center',
    fontSize: '16px',
    color: chineseBlack,
    lineHeight: '100%',
    '& span': {color: eucalyptus, fontWeight: 'bold', marginRight: '10px'},
  },
  alignSelection: {
    flex: 1,
  },
  selectionInfosContainer: {
    width: '100%',
    marginTop: '20px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  containerCards: {
    minHeight: '60vh',
    width: '95%',
    marginTop: '50px',
    marginBottom: '90px',

    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    gap: '32px',
  },
  buttonContainerMob: {
    marginTop: '20px',
    width: '100%',
    height: '90px',
    position: 'fixed',
    bottom: '0',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  "@media (max-width: 1445px)": {containerCards: {width: '97%'}},
  "@media (max-width: 900px)": {
    headerPage: {
      '& img': {
        padding: '60px 0px',
        width: '180px',
      },
    },
    buttonGoBack: {
      '& img': {
        marginRight: '20px',
        width: '14px',
      },
    },
    textSelection: {
      textAlign: 'justify',
      marginTop: '0px',
    },
    textSelectionCounter: {
      textAlign: 'justify',
      marginTop: '0px',
      padding: '10px',
    },
    tittleSelection: {fontSize: '24px', marginBottom: '20px'},
    selectionInfosContainer: {marginTop: '16px'}
  },

  "@media (max-width: 1120px)":{
    headerPage: { '& img': {marginBottom: '-20px'} },
    tittleSelection: {fontSize: '24px', marginBottom: '40px'},
    textSelection: {width: '100%'},
    textSelectionCounter: {
      width: '100%',
      padding: '10px 0px',
      backgroundColor: '#fff',
      position: 'sticky',
      top: '190px',
      marginBottom: '-20px',
    },
    containerCards: {width: '100%', gap: '12px', justifyContent: 'center'},
  }
});

export default useStyles;

