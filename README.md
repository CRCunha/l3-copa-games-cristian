<h1 align="center">Welcome to l3-copa-games-cristian 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-0.0.1-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> Projeto de teste técnico destiano a lambda3


### ✨ [Demo](http://cristianapp.com.br.s3-website-sa-east-1.amazonaws.com)

## Install

```sh
yarn install
```


> [GitLab Repo Link](https://gitlab.com/CRCunha/l3-copa-games-cristian)

## Author

👤 **Cristian Raffi Cunha**

* Website: cristiancunha.com
* Github: [@CRCunha](https://github.com/CRCunha)
* LinkedIn: [@Cristian Raffi Cunha](https://www.linkedin.com/in/cristian-raffi-cunha/)

## Prints
***


<img src="https://i.imgur.com/rcYplsT.png" style="height: 100px; width:100px;"/>
<img src="https://i.imgur.com/IrldBRH.png" style="height: 100px; width:100px;"/>
<img src="https://i.imgur.com/xmmoR46.png" style="height: 100px; width:100px;"/>
<img src="https://i.imgur.com/ciUvlXJ.png" style="height: 100px; width:100px;"/>
